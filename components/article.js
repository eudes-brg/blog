import React from "react"
import { formatDate } from "../utils/formatDate"

import Image from "next/image"
import CustomLink from "./customLink"

class Article extends React.Component{
    constructor (props) {
        super(props)
        this.formatDate = formatDate.bind(this)
    }


    render() {
        return (
            <div className="my-6">
                <div className="relative w-full h-96 py-6">
                    
                    <CustomLink 
                        text={
                            <Image
                                src={`/initial/${this.props.image}`}
                                alt="article"
                                layout="fill"
                                objectFit="cover"
                                objectPosition={'center'}
                                quality={100}
                                priority
                            />
                        }
                        location={`/${this.props.slug}`}
                        styles="relative w-full block h-full"

                    />
                </div>
                <h2 className="font-serif font-medium text-center uppercase text-2xl pt-6">
                    
                    <CustomLink text={this.props.titre} location={`/${this.props.slug}`} styles="" />
                </h2>
                <p className="py-4 font-serif text-slate-500">
                    by 
                    <span className="uppercase mx-2">{this.props.auteur}</span>
                    |
                    <span className="mx-2">{this.formatDate(this.props.date)}</span>
                    |
                    <span className="mx-2">COMMENT ENVOUTER UN HOMME</span>
                    ,
                    <span>COMMENT ENVOUTER UNE FEMME</span>
                    ,
                    <span>COMMENT EVITER LES RUPTURES ET DIVORCES DANS LES COUPLES</span>
                    ,
                    <span>retour affectif</span>
                    ,
                    <span>retour d&aposaffection</span>
                </p>
                <div>
                    <div className="font-serif mb-4">
                        <p className="text-lg font-medium text-slate-600">
                            {this.props.appercu}
                        </p>
                    </div>
                    <CustomLink text='lire plus' location={`/${this.props.slug}`} styles="mt-4 hover:text-amber-500" />
                </div>
            </div>
        )
    }
}

export default Article