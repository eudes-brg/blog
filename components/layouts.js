import Head from "next/head";

import TopHeader from "./topHeader";
import BottomHeader from "./bottomHeader";
import Footer from "./footer";


export default function Layout({children}) {
    return (
        <>
            <Head>
                <meta name="description" content="Learn how to build a personal website using Next.js"/>
            </Head>
            <header>
                <TopHeader />
                <BottomHeader />
            </header>
            <main>
                {children}
            </main>
            <Footer />
        </>
    )
}