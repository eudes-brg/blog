import React from "react";
import Head from "next/head"
import Image from "next/image";
import Welcome from "../public/initial/bienvenue.png"
import Medium from "../public/initial/medium.jpg"
import DatePicker from "sassy-datepicker";
import CustomLink from "../components/customLink";
import { getPhone,getLocalisation,getAllCategory } from "../api/parseRequest";


import Layout from "../components/layouts"
import Article from "../components/article";

class  Home extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      phone: '',
      localisation : '',
      articles : [],
      categories: [],
    }
  }

  async componentDidMount () { 
    //récupération du numéro de téléphone
    const phone = await getPhone() 
    const localisation = await getLocalisation()
    const categories = await getAllCategory()
    this.setState({
      phone : phone,
      localisation : localisation,
      categories : categories 
    })
  }
  

  render () {
    console.log(this.state.categories);
    return (
      <div>
        <Head>
          <title>Accueil-le plus grand maître compétent et puissant du monde papa SESSOU</title>
        </Head>
        <Layout>
          <div className="bg-white my-11">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div className="flex items-center flex-row justify-center relative my-4 py-8">
                    <Image 
                      src={Welcome}
                      alt="welcome"
                    />
                </div>
            </div>
          </div>

          <div className="bg-white my-8">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-4">
                <div className="grid md:grid-cols-3 grid-rows-1 gap-x-8 place-items-center">
                    <div>
                        <Image 
                            src={Medium}
                            alt="medium"
                        />
                        <p className="uppercase">le puissant maître medium SESSOU</p>
                        <p className="uppercase font-light">{this.state.localisation}</p>
                    </div>
                    <div className="md:col-span-2 sm:px-6 lg:px-8">
                        <h1 className="uppercase font-semibold text-2xl text-center mx-6">
                            le plus grand maître médium compétent et puissant du monde papa SESSOU 
                            téléphone: {this.state.phone}(Whatsapp | Appel)
                        </h1>
                        <p className="text-center text-lg my-8 mx-6">
                            Spécialistes des Problèmes Affectifs en tout genres, 
                            PAPA SESSOU est un Expert ROI DU BENIN et intervient dans plusieurs 
                            domaines et cela depuis plusieurs années grâce à ses dons ancestraux. 
                        </p>
                        <p className="text-lg mx-6 ">
                            NB: Retrouvez satisfaction à tous vos problèmes par la grâce de mes génies
                        </p>
                    </div>
                </div>
            </div>
          </div>

          <div className="bg-white my-11">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
              <div className="grid md:grid-cols-3 grid-cols-1 justify-items-center py-6">
                <div className="w-full md:col-span-2 sm:px-6 lg:px-8">
                  <div className="grid md:grid-cols-2 md:gap-x-11 justify-items-center ">
                    <div className="py-4 my-4">
                        <span className="p-4 border-2 text-red-600 border-red-500">Pourquoi ce blog?</span>
                    </div>
                    <div className="py-4 my-4">
                        <CustomLink  
                            location="/about" 
                            text="Qui est SESSOU" 
                            styles="p-4 border-2 text-red-600 border-red-500"
                        />
                    </div>
                  </div>
                </div>
                <div className="mt-6 font-serif">
                  <div className="my-2">
                    <h2 className="font-semibold text-xl pb-4 text-center">
                      A propos de Maître SESSOU
                    </h2>
                    <p className="text-lg">
                      Bienvenu,
                      <br />
                      je suis grand médium SESSOU. Ma mission à travers ce blog, est d’établir simplement 
                      et facilement un contact entre les hommes ( comme vous ) qui sont à la quête de 
                      solution mystique efficaces et sans conséquences.
                    </p>
                  </div>
                  <div className="my-2">
                    <h2 className="font-semibold text-xl pb-4 text-center">
                      C’est votre première visite sur mon blog ?
                    </h2>
                    <p className="text-lg">
                      Il y a une opportunité de 11% pour vous, mais vous ne l’avez pas vite saisie. 
                      A la prochaine et que mes Dieux et Génies soient continuellement avec vous.
                    </p>
                  </div>
                </div>
              </div>
              <div className="grid md:grid-cols-3 grid-cols-1 justify-items-center py-6">
                <div className="w-full md:col-span-2 sm:px-6 lg:px-8">
                  <div className="grid grid-cols-1 justify-items-center">
                    {
                      this.state.articles.map((item)=>{
                        console.log(item)
                        return (
                          <Article 
                            key={item.id}
                            titre={item.get("titre")}
                            auteur={item.get("auteur")}
                            image={item.get("image")}
                            categoryID={item.get("categoryID")}
                            appercu={item.get("appercu")}
                            date={item.createdAt}
                            slug={item.get("slug")}
                          />
                        ) 
                      })
                    }
                  </div>
                </div>
                <div className="w-full">
                    <div className="grid grid-cols-1 justify-items-center px-8">
                        <div className="py-4 px-4 w-full">
                            <h4 className="mb-4 w-full">Catégories</h4>
                            <form className="w-full">
                                <select name="category" className="py-2 bg-white border-2 focus:outline-0 w-full">
                                    <option>Sélectionner une catégorie</option>
                                    {
                                      this.state.categories.map((item)=>{
                                        return <option key={item.id}>{item.get("nom")}</option>
                                      })
                                    }
                                </select>
                            </form>
                        </div>
                        <div className="py-4 px-4 w-full">
                            <form className="border-2 bg-slate-200 w-full">
                                <input className="w-2/3 pl-2 h-10 bg-white border-r-2 focus:outline-0" type="text" placeholder="Rechercher"/>
                                <input className="w-1/3 h-10 px-px bg-slate-200" type={"submit"} value="Rechercher"/>
                            </form>
                        </div>
                        <div className="py-4">
                            <h4 className="text-xl text-center">Articles recents</h4>
                            <ul className="text-center">
                                {
                                  this.state.articles.slice(0,9).map((item)=>{
                                    console.log(item)
                                    return (
                                      <li key={item.id} className='lowercase'>
                                        <CustomLink text={item.get('titre')} location={`/${item.get('slug')}`} styles='text-amber-500' />
                                      </li>
                                    )
                                  })
                                }
                            </ul>
                        </div>
                        <div className="py-4">
                            <DatePicker onChange={this.onChange} />
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      </div>
    )

  }
}

export default Home


