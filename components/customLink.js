import Link from "next/link";

export default function CustomLink(props) {
    return (
        <Link href={props.location}>
            <a className={props.styles}>{props.text}</a>
        </Link>
    )
}