import Parse from "parse"
import { PARSE_APP_ID,PARSE_JS_KEY,PARSE_SERVER_URL } from "../config"

Parse.initialize(PARSE_APP_ID,PARSE_JS_KEY)
Parse.serverURL = PARSE_SERVER_URL


//début des modifications de l'émail

export async function getEmail() {
    //instation de la classe blog
    const blog = Parse.Object.extend("blog")
    const blogQuery = new Parse.Query(blog)

    try {
        const blogData = await blogQuery.first()
        
        //récupération de l'émail
        const email = blogData.get('email')
        return email
    } catch (error) {
        console.log(`Failed to retrieve the object, with error code: ${error.message}`);
    }
}

export async function changeEmail(emailUpdate) {
    //instation de la classe blog
    const blog = Parse.Object.extend("blog")
    const blogQuery = new Parse.Query(blog)
    try {
        const blogData = await blogQuery.first()
        blogData.set("email",emailUpdate)
        let result = await blogData.save()
        return result.get("email") 
    } catch (error) {
        alert('Failed to update object, with error code: ' + error.message);
    }
}

//fin des modifications de l'émail



//débuts des modifications du phone


export async function getPhone() {
    //instation de la classe blog
    const blog = Parse.Object.extend("blog")
    const blogQuery = new Parse.Query(blog)

    try {
        const blogData = await blogQuery.first()
        
        //récupération du phone
        const phone = blogData.get('phone')
        return phone
    } catch (error) {
        console.log(`Failed to retrieve the object, with error code: ${error.message}`);
    }
}

export async function changePhone(phoneUpdate) {
    //instation de la classe blog
    const blog = Parse.Object.extend("blog")
    const blogQuery = new Parse.Query(blog)
    try {
        const blogData = await blogQuery.first()
        blogData.set("phone",phoneUpdate)
        let result = await blogData.save()
        return result.get("phone") 
    } catch (error) {
        alert('Failed to update object, with error code: ' + error.message);
    }
}

//fin des modifications du phone

//débuts des modifications de la localisation


export async function getLocalisation() {
    //instation de la classe blog
    const blog = Parse.Object.extend("blog")
    const blogQuery = new Parse.Query(blog)

    try {
        const blogData = await blogQuery.first()
        
        //récupération de la localisation
        const localisation = blogData.get('localisation')
        return localisation
    } catch (error) {
        console.log(`Failed to retrieve the object, with error code: ${error.message}`);
    }
}

export async function changeLocalisation(localisationUpdate) {
    //instation de la classe blog
    const blog = Parse.Object.extend("blog")
    const blogQuery = new Parse.Query(blog)
    try {
        const blogData = await blogQuery.first()
        blogData.set("localisation",localisationUpdate)
        let result = await blogData.save()
        return result.get("localisation") 
    } catch (error) {
        alert('Failed to update object, with error code: ' + error.message);
    }
}

//fin des modifications de la localisation

//debut des modifications des categories

export async function getAllCategory() {
    const category = Parse.Object.extend("categorie")
    const categoryQuery = new Parse.Query(category) 

    try {
        const categories = await categoryQuery.findAll()
        return categories
    } catch (error) {
        console.log(`Failed to retrieve the object, with error code: ${error.message}`);
    }
}

export async function getCategory(objectID) {
    
}

export async function createCategory(attributes) {
    //instaciation de la classe Categorie
    const category = new Parse.Object('categorie')

    //Définition des attributs
    category.set("nom",attributes.nom)
    category.set("slug",attributes.slug)
    try{
        //sauvegarde de l'object
        let result = await category.save()
        console.log(result)
    }catch(error){
        alert('Failed to create new object, with error code: ' + error.message);
    }

}

export async function UpdateCategory(objectID,attributes) {
    
}

export async function deleteCategory(objectID) {
    const category = Parse.Object.extend("categorie")
    const categoryQuery = new Parse.Query(category)
    categoryQuery.equalTo("objectId",objectID)
    try{
        const categoryDelete = await categoryQuery.first()
        //destroy the object
        let result = await categoryDelete.destroy();
        return result
    }catch(error){
        alert('Failed to delete object, with error code: ' + error.message);
    }
}

//fin des modifications des categories


//debut des modifications des articles

export async function getAllArticle() {
    const article = Parse.Object.extend("article")
    const articleQuery = new Parse.Query(article) 

    try {
        const articles = await articleQuery.findAll()
        return articles
    } catch (error) {
        console.log(`Failed to retrieve the object, with error code: ${error.message}`);
    }
}

export async function getArticle(objectID) {
    
}

export async function createArticle(attributes) {
    //instaciation de la classe Categorie
    const article = new Parse.Object('article')

    //Définition des attributs
    article.set("nom",attributes.nom)
    article.set("slug",attributes.slug)
    try{
        //sauvegarde de l'object
        let result = await category.save()
        console.log(result)
    }catch(error){
        alert('Failed to create new object, with error code: ' + error.message);
    }

}

export async function UpdateArticle(objectID,attributes) {
    
}

export async function deleteArticle(objectID) {
    const article = Parse.Object.extend("article")
    const articleQuery = new Parse.Query(article)
    articleQuery.equalTo("objectId",objectID)
    try{
        const articleDelete = await articleQuery.first()
        //destroy the object
        let result = await articleDelete.destroy();
        return result
    }catch(error){
        alert('Failed to delete object, with error code: ' + error.message);
    }
}

//fin des modifications des articles



//debut des modifications des Témognages

export async function getAllTestimony() {
    const category = Parse.Object.extend("categorie")
    const categoryQuery = new Parse.Query(category) 

    try {
        const categories = await categoryQuery.findAll()
        return categories
    } catch (error) {
        console.log(`Failed to retrieve the object, with error code: ${error.message}`);
    }
}

export async function getTestimony(objectID) {
    
}

export async function createTestimony(attributes) {
    //instaciation de la classe Categorie
    const category = new Parse.Object('categorie')

    //Définition des attributs
    category.set("nom",attributes.nom)
    category.set("slug",attributes.slug)
    try{
        //sauvegarde de l'object
        let result = await category.save()
        console.log(result)
    }catch(error){
        alert('Failed to create new object, with error code: ' + error.message);
    }

}

export async function UpdateTestimony(objectID,attributes) {
    
}

export async function deleteTestimony(objectID) {
    const category = Parse.Object.extend("categorie")
    const categoryQuery = new Parse.Query(category)
    categoryQuery.equalTo("objectId",objectID)
    try{
        const categoryDelete = await categoryQuery.first()
        //destroy the object
        let result = await categoryDelete.destroy();
        return result
    }catch(error){
        alert('Failed to delete object, with error code: ' + error.message);
    }
}

//fin des modifications des Témognages