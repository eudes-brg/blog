import React from 'react'
import AdminArticleItem from './adminArticleItem'
import {getAllCategory, getAllArticle,deleteArticle} from '../api/parseRequest'
import {formatDate} from '../utils/formatDate'
import { formatSlug } from '../utils/formatSlug'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'


class AdminShowArticle extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            articles : [],
            categories : [],
            isOpenModal : false,
            listOption : [],
            createArticleContent : '',
            createArticleTitre : '',
            createArticleAuteur : '',
            createArticleImage : '',
            imageCreateUrl : null,
            createArticleCategory : []
        }
    }

    async deleteOneArticle (objectId,id) {
        //supression de l'article avec l'identifiant objectID
        const result = await deleteArticle(objectId)
        //trouver la position de la catégorie à supprimer dans la variable this.state.catégories
        console.log(this.state.articles[id])
        console.log(objectId)
        console.log(result)
        const articles = await getAllArticle()
        this.setState({
            articles : articles,
        })
    }
    
    buildListOption () {
        let options = []
        this.state.categories.map((item,index)=>{
            options [index] = {
                value : item.id,
                label : item.get('nom')
            }
        })
        this.setState({
            listOption : options,
        })
    }

    uploadToClient(event) {
        if(event.target.files && event.target.files[0]) {
            const i = event.target.files[0];
            this.setState({
                createArticleImage : i,
                imageCreateUrl : URL.createObjectURL(i)
            })

        }
    } 


    async uploadToServer() {
        const body = new FormData()
        body.append("file", image);
        const response = await fetch("/api/upload", {
            method: "POST",
            body
        });
    }

    async createOneArticle () {
        if(this.state.createArticleTitre != "" && this.state.createArticleImage != '' && this.state.createArticleContent != '' && this.state.createArticleCategory != [] && this.state.createArticleCategory.length != 0) {
            let articleTitre = this.state.createArticleTitre 
            let articleSlug = formatSlug(this.state.createArticleTitre )
            let articleAuteur = this.state.createArticleAuteur
        } else {
            console.log('tout n\'y est pas')
        }
        
        //console.log('titre : ',this.state.createArticleTitre)
        //console.log('Auteur : ',this.state.createArticleAuteur)
        //console.log('image : ',this.state.createArticleImage)
        //console.log('posts : ',this.state.createArticleContent)
        //console.log('category :',this.state.createArticleCategory)
        //const data = new FormData()
        //data.append('file',this.state.createArticleImage)
        //const response = await fetch("/api/hello",{
        //    method: "POST",
        //    body : data,
        //})
        //console.log(await response.json())
        
        
    }

    async componentDidMount () {
        const articles = await getAllArticle()
        const categories = await getAllCategory()
        this.setState({
            articles : articles,
            categories : categories,
        })
    }

    render () {
        const animatedComponents = makeAnimated()
        return (
            <>
                <div className="flex justify-between items-center border-gray-300">
                    <h1 className="text-xl font-semibold  pt-2 pb-6">Categories</h1>
                    <button 
                        onClick={()=>{
                            this.buildListOption()
                            this.setState({isOpenModal : true})
                            
                        }} 
                        className='bg-blue-500 p-2 text-white rounded capitalize'> 
                        Nouveau Article
                    </button>
                </div>
                <div className="bg-white shadow rounded-sm my-2.5 overflow-x-auto">
                    <table className="min-w-max w-full table-auto">
                        <thead>
                            <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th className="py-3 px-6 text-left">identifiant</th>
                                <th className="py-3 px-6 text-left">titre</th>
                                <th className="py-3 px-6 text-left">Auteur</th>
                                <th className="py-3 px-6 text-left">Date</th>
                                <th className="py-3 px-6 text-center">Appercu</th>
                                <th className="py-3 px-6 text-center">image</th>
                                <th className='py-3 px-6 text-center'>posts</th>
                                <th className="py-3 px-6 text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody className="text-gray-600 text-sm">
                            {this.state.articles.map((item,index)=>{
                                return (
                                    <AdminArticleItem 
                                        key={item.id} 
                                        id={item.id} 
                                        titre={item.get('titre')} 
                                        auteur='Marabout SESSOU' 
                                        date={formatDate(item.createdAt)}
                                        appercu={item.get("appercu")} 
                                        image={item.get("image")}
                                        posts={item.get("contenu")}
                                        handleDeleteArticle={()=>this.deleteOneArticle(item.id,index)}
                                    />
                                ) 
                            })}
                        </tbody>
                    </table>
                    {this.state.isOpenModal ? (
                        <>
                            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto absolute top-0 right-0 left-0 z-50 outline-none focus:outline-none">
                                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                    <h3 className="text-3xl font-semibold mr-6">
                                        Créer un Article
                                    </h3>
                                    <button className="p-1 ml-auto float-right" onClick={() => this.setState({isOpenModal : false})}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                    <form method="post" encType="multipart/form-data">
                                        <div className="mb-3 pt-0">
                                            <label>Titre de l&apos;Article</label>
                                            <input
                                                onChange={(event)=>this.setState({
                                                    createArticleTitre : event.target.value
                                                })}
                                                value={this.state.createArticleTitre}
                                                type="text" 
                                                placeholder="" 
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                        
                                        <div className="mb-3 pt-0">
                                            <label>Nom de l&apos;auteur de  l&apos;Article</label>
                                            <input
                                                onChange={(event)=>this.setState({
                                                    createArticleAuteur : event.target.value
                                                })}
                                                value={this.state.createArticleAuteur}
                                                type="text" 
                                                placeholder="slug categorie" 
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Image de l&apos;Article</label>
                                            <div className="grid grid-cols-3">
                                                <div className='col-span-2'>
                                                    <input
                                                        accept={'.jpg, .jpeg, .png'}
                                                        onChange={(event)=>this.uploadToClient(event)}
                                                        type="file"
                                                        className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                                    />
                                                </div>
                                                <div>
                                                    <img src={this.state.imageCreateUrl} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label htmlFor="" className="block mb-2 text-sm text-gray-900">Votre Article</label>
                                            <textarea
                                                onChange={(event)=>{this.setState({
                                                    createArticleContent : event.target.value
                                                })}}
                                                value={this.state.createArticleContent} 
                                                id="" 
                                                rows="4" 
                                                className="outline-none block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 " 
                                                placeholder="contenu de l'article..."
                                            ></textarea>
                                        </div>
                                        <div className='mb-3 pt-0'>
                                            <label>Sélectionner les catégories</label>
                                            <Select 
                                                isMulti 
                                                components={animatedComponents}
                                                placeholder="Sélectionner une Catégorie"
                                                options={this.state.listOption} 
                                                name="category"
                                                value={this.state.createArticleCategory}
                                                onChange={(event)=>{
                                                    this.setState({
                                                    createArticleCategory : event
                                                })
                                                console.log(event)
                                            }
                                                
                                            }
                                            />
                                        </div>
                                    </form>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                                    <button
                                        className="text-red-500 background-transparent font-bold px-6 py-2 text-sm mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={() => this.setState({isOpenModal : false})}
                                    >
                                        Annuler
                                    </button>
                                    <button
                                        className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={() =>{
                                            this.createOneArticle()
                                            this.setState({isOpenModal : false})
                                        }}
                                    >
                                        Créer l&apos;article
                                    </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                        </>
                    ):null}
                </div>
            </>
        )
    }
}

export default AdminShowArticle