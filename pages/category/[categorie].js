import React from "react";
import Layout from "../../components/layouts";
import Parse from 'parse'
import {PARSE_APP_ID,PARSE_JS_KEY,PARSE_SERVER_URL} from '../../config'


class CategoryPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            articlesCategory : ''
        }
        Parse.initialize(PARSE_APP_ID,PARSE_JS_KEY)
        Parse.serverURL = PARSE_SERVER_URL
    }

    static async getInitialProps (context) {
        const {asPath,query,pathname} = context
        return {asPath,query,pathname}
    }

    async componentDidMount () {
        let Categories = Parse.Object.extend('')
        let query = new Parse.Query(Categories)
        try {
            
        } catch (error) {
            console.log("Error: " + error.code + " " + error.message); 
        }
    }

    render () {
        return (
            <Layout>
                
            </Layout>
        )
    }
}

export default CategoryPage