import React from 'react'
import {getEmail,changeEmail} from '../api/parseRequest'


class AdminShowEmail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            email : '',
            isOpenEditModal : false,
            emailChangeValue : '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        this.setState({
            emailChangeValue : event.target.value
        })
    }

    async handleSubmit() {
        const emailChanged = await changeEmail(this.state.emailChangeValue)
        this.setState({
            email : emailChanged,
        })
    }

    async componentDidMount () {
        const email = await getEmail()
        this.setState({
            email : email,
        })
    }

    render () {
        return (
            <>
                <div className="flex justify-between uppercase items-center border-gray-300">
                    <h1 className="text-xl font-semibold pt-2 pb-6">{'cool'}</h1>
                    <button onClick={()=>console.log('création de l"article')} className='bg-blue-500 p-2 text-white rounded capitalize'> 
                        cool
                    </button>
                </div>
                <div className="bg-white shadow rounded-sm my-2.5 overflow-x-auto">
                    <table className="min-w-max w-full table-auto">
                        <thead>
                            <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th className="py-3 px-6 text-left">Email</th>
                                <th className="py-3 px-6 text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody className="text-gray-600 text-sm">
                            <tr className="border-b border-gray-200 hover:bg-gray-100">
                                <td className="py-3 px-6 text-left whitespace-nowrap">
                                    {this.state.email}
                                </td>
                                <td className="py-3 px-6 text-center">
                                    <div className="flex item-center justify-center">
                                        <div onClick={()=>{alert('aller voir sa modification')}} className="w-4 mx-2 transform hover:text-purple-500 hover:scale-110 cursor-pointer cursor-pointer">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                            </svg>
                                        </div>
                                        <div onClick={()=>this.setState({isOpenEditModal: true})} className="w-4 mx-2 transform hover:text-purple-500 hover:scale-110 cursor-pointer cursor-pointer">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                            </svg>
                                        </div>
                                    </div>
                                </td>
                            </tr>           
                        </tbody>
                    </table> 
                    {this.state.isOpenEditModal ? (
                        <>
                        <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                            <div className="relative w-auto my-6 mx-auto max-w-3xl">
                            {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                        <h3 className="text-xl font-semibold">
                                            Changer l&apos;Email
                                        </h3>
                                        <button onClick={() =>this.setState({isOpenEditModal: false}) } className="p-1 ml-auto float-right">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                            </svg>
                                        </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                        <form>
                                            <div className="mb-3 pt-0">
                                                <input 
                                                    onChange={this.handleChange}
                                                    value={this.state.emailChangeValue}
                                                    type="email" 
                                                    placeholder="changer votre email" 
                                                    className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white  rounded text-sm border-2 outline-none focus:outline-none w-full" 
                                                    autoFocus
                                                />
                                            </div>
                                        </form>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                                        <button className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button" onClick={() =>this.setState({isOpenEditModal: false})}>
                                            Close
                                        </button>
                                        <button  
                                            className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" 
                                            type="button"
                                            onClick={() =>{
                                                this.handleSubmit()
                                                this.setState({isOpenEditModal: false})
                                            }}
                                        >
                                            Changer 
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                        </>
                    ) : null}
                </div>
            </>
        )
    }
}

export default AdminShowEmail