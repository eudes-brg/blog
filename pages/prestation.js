import React from "react";
import Image from "next/image";
import Layout from "../components/layouts";
import Photo from "../public/initial/Apropos.jpg"
import Medium from "../public/initial/medium.jpg"

class Prestation extends React.Component {
    constructor(props) {
        super(props)
    }

    render () {
        return (
            <Layout>
                <div classNameName="bg-white">
                    <div classNameName="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div classNameName=" flex justify-center">
                            <div classNameName="lg:w-2/3 md:w-1/2 bg-gray-300 rounded-lg overflow-hidden sm:mr-10 p-10 flex items-end justify-center relative">
                                <Image src={Photo} alt="photo contact" layout="fill" objectFit="cover" objectPosition={"center"}/>
                                <div classNameName="bg-transparent text-white relative flex flex-wrap py-6 w-4/5 justify-center">
                                    <h1 classNameName="text-5xl text-center">LES PRODUITS ET SERVICES DU MEDIUM SESSOU</h1>
                                    <div classNameName="lg:w-1/2 px-6 mt-4 lg:mt-0">
                                        <p classNameName="leading-relaxed">A travers ce blog, vous apprendrez d’avantage sur mes différents produit et services. Naviguez librement et apprenez d’avantage de nouvelles choses.</p>
                                        <p>Que mes puissants génies soient avec vous !!!</p>
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <section classNameName="text-gray-600 body-font">
                            <div classNameName="container px-5 py-24 mx-auto">
                                <div classNameName="flex flex-wrap -m-4">
                                <div classNameName="p-4 md:w-1/3">
                                    <div className="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden">
                                    <Image 
                                        className="lg:h-48 md:h-36 w-full object-cover object-center" 
                                        src={Medium}
                                        alt="blog"
                                    />
                                    <div className="p-6">
                                        <h2 className="tracking-widest text-xs title-font font-medium text-gray-400 mb-1">CATEGORY</h2>
                                        <h1 className="title-font text-lg font-medium text-gray-900 mb-3">The Catalyzer</h1>
                                        <p className="leading-relaxed mb-3">Photo booth fam kinfolk cold-pressed sriracha leggings jianbing microdosing tousled waistcoat.</p>
                                        <div className="flex items-center flex-wrap ">
                                        <a className="text-indigo-500 inline-flex items-center md:mb-2 lg:mb-0">Learn More
                                            <svg className="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                            <path d="M5 12h14"></path>
                                            <path d="M12 5l7 7-7 7"></path>
                                            </svg>
                                        </a>
                                        <span className="text-gray-400 mr-3 inline-flex items-center lg:ml-auto md:ml-0 ml-auto leading-none text-sm pr-3 py-1 border-r-2 border-gray-200">
                                            <svg className="w-4 h-4 mr-1" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" viewBox="0 0 24 24">
                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                            <circle cx="12" cy="12" r="3"></circle>
                                            </svg>1.2K
                                        </span>
                                        <span className="text-gray-400 inline-flex items-center leading-none text-sm">
                                            <svg className="w-4 h-4 mr-1" stroke="currentColor" strokeWidth="2" fill="none" strokeLinecap="round" strokeLinejoin="round" viewBox="0 0 24 24">
                                            <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                                            </svg>6
                                        </span>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default Prestation 