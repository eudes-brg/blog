export function formatDate(date) {
    if (date === undefined) {
        return 'loading'
    } else {
        return date.toString().split(' ').slice(1,4).join(' ')    
    }
    
}