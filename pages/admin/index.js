import React from "react"
import AdminShowEmail from "../../components/adminShowEmail"
import AdminShowPhone from "../../components/adminShowPhone"
import AdminShowLocalisation from "../../components/adminShowLocalisation"
import AdminShowCategory from "../../components/adminShowCategory"
import AdminShowArticle from "../../components/adminShowArticle"
import AdminShowTemoignage from "../../components/adminShowTemoignage"

class Admin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpenBlogInfoDrop : false,
            isItemCliked : 'article',
            mobileNav : false,
            displaymobileNavValue : '-translate-x-full'
        }
        this.toogleNav = this.toogleNav.bind(this)
    }

    toogleNav() {
        if (this.state.mobileNav) {
            this.setState({
                displaymobileNavValue : '-translate-x-full'
            })
        } else {
            this.setState({
                displaymobileNavValue : ''
            })
        }
    }

    async componentDidMount () {
        
    }

    _displayChoix (choix) {
        if (choix == 'article') {
            return <AdminShowArticle />
        }
        if (choix == 'email') {
            return <AdminShowEmail />
        }
        if (choix == 'phone') {
            return <AdminShowPhone />
        }
        if (choix == 'localisation') {
            return <AdminShowLocalisation />
        }
        if (choix == 'categorie') {
            return <AdminShowCategory />
        }
        if (choix == 'témoignages') {
            return <AdminShowTemoignage />
        }
    }

    render() {
        return (
            <div className='flex relative'>
                {/*<!-- NAV -->*/}
                <nav className={`absolute md:relative w-64 transform ${this.state.displaymobileNavValue}  md:translate-x-0 h-screen overflow-y-scroll bg-black transition-all duration-300`}>
                    <div className="flex flex-col justify-between h-full">
                        <div className="p-4">
                            {/*<!-- LOGO -->*/}
                            <a className="flex items-center text-white space-x-4" href="">
                                <svg className="w-7 h-7 bg-indigo-600 rounded-lg p-1" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4"></path></svg>
                                <span className="text-2xl font-bold">Better Code</span>
                            </a>
        
                            {/*<!-- SEARCH BAR -->*/}
                            <div className="border-gray-700 py-5 text-white border-b rounded">
                                <div className="relative">
                                    <div className="absolute inset-y-0 left-0 flex items-center pl-2">
                                        <svg className="w-5 h-5 text-gray-500" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                                        </svg>
                                    </div>
                                    <form action="" method="GET">
                                        <input type="search" className="w-full py-2 rounded pl-10 bg-gray-800 border-none focus:outline-none focus:ring-0" placeholder="Rechercher"/>
                                    </form>
                                </div>
                                {/*<!-- SEARCH RESULT -->*/}
                            </div>
        
                            {/*<!-- NAV LINKS -->*/}
                            <div className="py-4 text-gray-400 space-y-1">
                                {/*<!-- BASIC LINK -->*/}
                                <a href="#" className="block py-2.5 px-4 flex items-center space-x-2 bg-gray-800 text-white hover:bg-gray-800 hover:text-white rounded">
                                    <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
                                    <span>Tableau de bord</span>
                                </a>
                                {/*<!-- DROPDOWN LINK -->*/}
                                <div className="block">
                                    <div onClick={()=>this.setState({isOpenBlogInfoDrop : !this.state.isOpenBlogInfoDrop})} className="flex items-center justify-between hover:bg-gray-800 hover:text-white cursor-pointer py-2.5 px-4 rounded">
                                        <div  className="flex items-center space-x-2">
                                            <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4"></path></svg>
                                            <span className="mr-1">Blog info</span>
                                        </div>
                                        {this.state.isOpenBlogInfoDrop ? (
                                            <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 15l7-7 7 7"></path></svg>
                                        ):(
                                            <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7"></path></svg>    
                                        )}
                                        
                                    </div>
                                    {this.state.isOpenBlogInfoDrop ? (
                                        <div className="text-sm border-l-2 border-gray-800 mx-6 my-2.5 px-2.5 flex flex-col gap-y-1">
                                            <button onClick={()=>this.setState({isItemCliked : 'email'})} className="block py-2 px-4 hover:bg-gray-800 hover:text-white rounded text-left">
                                                Email
                                            </button>
                                            <button onClick={()=>this.setState({isItemCliked : 'phone'})} className="block py-2 px-4 hover:bg-gray-800 hover:text-white rounded text-left">
                                                Téléphone
                                            </button>
                                            <button onClick={()=>this.setState({isItemCliked : 'localisation'})} className="block py-2 px-4 hover:bg-gray-800 hover:text-white rounded text-left">
                                                Localisation
                                            </button>
                                        </div>
                                    ):('')}
                                </div>
                                <div className="block">
                                    <div onClick={()=>this.setState({isItemCliked : 'categorie'})} className="flex items-center justify-between hover:bg-gray-800 hover:text-white cursor-pointer py-2.5 px-4 rounded">
                                        <div className="flex items-center space-x-2">
                                            <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4"></path></svg>
                                            <span className="mr-1">Categorie</span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div className="block">
                                    <div onClick={()=>this.setState({isItemCliked : 'article'})} className="flex items-center justify-between hover:bg-gray-800 hover:text-white cursor-pointer py-2.5 px-4 rounded">
                                        <div className="flex items-center space-x-2">
                                            <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4"></path></svg>
                                            <span className="mr-1">Article</span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div className="block">
                                    <div onClick={()=>this.setState({isItemCliked : 'témoignages'})} className="flex items-center justify-between hover:bg-gray-800 hover:text-white cursor-pointer py-2.5 px-4 rounded">
                                        <div className="flex items-center space-x-2">
                                            <svg className="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M20 7l-8-4-8 4m16 0l-8 4m8-4v10l-8 4m0-10L4 7m8 4v10M4 7v10l8 4"></path></svg>
                                            <span className="mr-1">Témoignages</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                        {/*<!-- PROFILE -->*/}
                        <div className="text-gray-200 border-gray-800 rounded flex items-center justify-between p-2">
                            <div className="flex items-center space-x-3">
                                {/*<!-- AVATAR IMAGE BY FIRST LETTER OF NAME -->*/}
                                <img src="https://ui-avatars.com/api/?name=Habib+Mhamadi&size=128&background=ff4433&color=fff" className="w-7 w-7 rounded-full" alt="Profile"/>
                                <h1>User</h1>
                            </div>
                            <button  className="hover:bg-gray-800 hover:text-white p-2 rounded">
                                <form  action="" method="POST">
                                    <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path>
                                    </svg>            
                                </form>
                            </button>
                        </div>
        
                    </div>
                </nav>
                {/*<!-- END OF NAV -->*/}
            
                {/*<!-- PAGE CONTENT -->*/}
                <main className="flex-1 h-screen overflow-y-scroll overflow-x-hidden">
                    <div className="md:hidden justify-between items-center bg-black text-white flex">
                        <h1 className="text-2xl font-bold px-4">PAPA SESSOU</h1>
                        <button onClick={()=>{
                            this.setState({
                                mobileNav : !this.state.mobileNav
                            })
                            this.toogleNav()
                        }}  className="btn p-4 focus:outline-none hover:bg-gray-800">
                            <svg className="w-6 h-6 fill-current" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16m-7 6h7"></path></svg>
                        </button>
                    </div>
                    <section className="max-w-7xl mx-auto py-4 px-5">
                        <div className="flex justify-between items-center border-b border-gray-300">
                            <h1 className="text-2xl font-semibold pt-2 pb-6">Dashboard</h1>
                        </div>

                        {this._displayChoix(this.state.isItemCliked)}
        
                        
                    </section>
                    {/*<!-- END OF PAGE CONTENT -->*/}
                </main>
            </div>
        )
    }
}

export default Admin