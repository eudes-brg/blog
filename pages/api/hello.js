// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const formidable = require('formidable');
const mv = require('mv')

export const config = {
  api: {
     bodyParser: false,
  }
};

export default function handler(req, res) {
  if (req.url === '/api/hello' && req.method.toLowerCase() === 'post') {
    const form = formidable({ multiples: true, uploadDir: __dirname });
    form.parse(req, (err, fields, files) => {
      console.log('fields:', fields);
      console.log('files:', files);
      var oldPath = files.file.filepath
      var newPath = `./public/articles/${files.file.originalFilename}`
      mv(oldPath, newPath, function(err) {
      });
      res.status(200).json({ fields, files })
    });
  }
}
