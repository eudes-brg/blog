import React from "react";
import Layout from "../../components/layouts";
import DatePicker from "sassy-datepicker";
import Image from "next/image";
import {PARSE_APP_ID,PARSE_JS_KEY,PARSE_SERVER_URL} from "../../config"
import Parse from "parse"
import { formatDate } from "../../utils/formatDate";

export class ArticlePage extends React.Component {
    constructor(props) {
        super(props)
        this.formatDate = formatDate.bind(this)
        this.state = {
            articleInformation : ''
        }
        Parse.initialize(PARSE_APP_ID,PARSE_JS_KEY)
        Parse.serverURL = PARSE_SERVER_URL
    }

    static async getInitialProps (context) {
        const {asPath,query,pathname} = context
        return {asPath,query,pathname}
    }

    async componentDidMount () {
        let article = Parse.Object.extend("Article")
        let query = new Parse.Query(article)
        try {
            query.equalTo('slug',this.props.query.titre)
            const resultat = await query.find();
            console.log(resultat[0].get('createdAt'))
            this.setState({
                articleInformation : {
                    auteur : resultat[0].get('auteur'),
                    date : resultat[0].get('createdAt'),
                    image : resultat[0].get('image'),
                    titre : resultat[0].get('titre'),
                    description : resultat[0].get('description')
                }
            })
            
        } catch (error) {
            console.log("Error: " + error.code + " " + error.message); 
        }
    }

    render () {
        return (
            <Layout>
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="grid md:grid-cols-3 grid-cols-1 justify-items-center py-6">
                    <div className="w-full md:col-span-2 sm:px-6 lg:px-8">
                    <div className="grid grid-cols-1 pt-8 ">
                        <h1 className="text-3xl font-light font-serif uppercase tracking-wide mb-4">{this.state.articleInformation.titre}</h1>
                        <p className="mb-6">
                            Par 
                            <span className="mx-1">{this.state.articleInformation.auteur}</span> |
                            <span className="mx-1">{this.formatDate(this.state.articleInformation.date)}</span> |
                            <span className="mx-1">Nombres commentaires</span>
                        </p>
                        <div className="grid grid-cols-1 justify-items-center py-4">
                            <div className="relative bg-slate-500 h-80 w-80">
                                <Image 
                                    src={`/initial/${this.state.articleInformation.image}`}
                                    alt='article image'
                                    layout='fill'
                                    priority
                                />
                            </div>
                            <div className="w-80 text-left uppercase font-semibold text-gray-500 mt-4">{this.state.articleInformation.titre}</div>
                        </div>
                        <div className="py-8 ">
                            <p className="text-center tracking-wider text-slate-600">{this.state.articleInformation.description}</p>
                        </div>
                        <div>
                            <span className="my-4 text-2xl">Poster un Commentaire</span>
                            <p className="text-slate-600 mb-2">
                                <span className="mr-mr">Votre adresse e-mail ne sera pas publiée.</span>
                                Les champs obligatoires sont indiqués avec *
                            </p>
                            <form className="text-slate-500" method="post">
                                <textarea 
                                    className="bg-slate-200 w-full h-52 p-2 outline-none"
                                    name="comment"
                                    maxLength={65525} 
                                    defaultValue='Commentaire*'
                                    required='required'
                                >
                                    
                                </textarea>
                                
                                <div className="my-4 h-10">
                                    <input className="bg-slate-200 w-full md:w-1/2 h-full pl-2 py-2 outline-none" name="nom" type={'text'} placeholder='Nom*' />
                                </div>
                                <div className="my-4 h-10">
                                    <input className="bg-slate-200 w-full md:w-1/2 h-full pl-2 py-2 outline-none" name="email" type={'email'} placeholder='Email*'/>
                                </div>
                                <div>
                                    <input className="mr-3"  type={'checkbox'} name='commentCookies' id="commentCookies"/>
                                    <label htmlFor="commentCookies">Enregistrer mon nom, mon e-mail et mon site dans le navigateur pour mon prochain commentaire.</label>
                                </div>
                                <div className="py-4 flex justify-end">
                                    <button type="submit" className="p-4 border-2 text-xl rounded text-blue-500">
                                        Poster le Commentaire
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                    <div className="w-full">
                        <div className="grid grid-cols-1 justify-items-center border-l-2">
                            <div className="py-4">
                                <h4 className="mb-4">Catégories</h4>
                                <form className="">
                                    <select name="category" className="py-2 bg-white border-2 focus:outline-0">
                                        <option>Sélectionner une catégorie</option>
                                        <option>autres domaines de travailles</option>
                                        <option>COMMENT ENVOUTER UN HOMME</option>
                                        <option>COMMENT ENVOUTER UNE FEMME</option>
                                        <option>retour d’affection</option>
                                    </select>
                                </form>
                            </div>
                            <div className="py-4">
                                <form className="border-2 bg-slate-200">
                                    <input className="pl-2 h-10 bg-white border-r-2 focus:outline-0" type="text" placeholder="Rechercher"/>
                                    <input className="h-10 px-px bg-slate-200" type={"submit"} value="Rechercher"/>
                                </form>
                            </div>
                            <div className="py-4">
                                <h4>Articles recents</h4>
                                <ul>
                                    <li>Article recents</li>
                                    <li>Article recents</li>
                                    <li>Article recents</li>
                                    <li>Article recents</li>
                                    <li>Article recents</li>
                                </ul>
                            </div>
                            <div className="py-4">
                                <h4>Commentaires recents</h4>
                                <ul>
                                    <li>Commentaire recent</li>
                                    <li>Commentaire recent</li>
                                    <li>Commentaire recent</li>
                                    <li>Commentaire recent</li>
                                    <li>Commentaire recent</li>
                                </ul>
                            </div>
                            <div className="py-4">
                                <DatePicker onChange={this.onChange} />
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default ArticlePage