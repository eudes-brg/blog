import React from "react";
import dynamic from "next/dynamic";

var Editor = dynamic(()=>import('./editor'),{
    ssr: false
})


class AdminArticleItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpenEditModal : false,
            articleTitre : this.props.titre,
            articleAuteur : this.props.auteur,
            articleAppercu : this.props.appercu,
            articlePosts : this.props.posts,
            articleTitreChange: this.props.titre,
            articleAuteurChange : this.props.auteur,
            articleAppercuChange : this.props.appercu,
            articlePostsChange : this.props.posts,
        }
    }

    async handleDeleteCurrentArticle () {
        this.props.handleDeleteArticle()
    }


    render () {
        return (
            <>
                <tr className="border-b border-gray-200 hover:bg-gray-100">
                    <td className="py-3 px-6 text-left whitespace-nowrap">
                        {this.props.id}
                    </td>
                    <td className="py-3 px-6 text-center">
                        {this.state.articleTitre}
                    </td>
                    <td className="py-3 px-6 text-center">
                        {this.state.articleAuteur}
                    </td>
                    <td className="py-3 px-6 text-center">
                        {this.props.date}
                    </td>
                    <td className="py-3 px-6 text-justify">
                        {this.props.appercu}
                    </td>
                    <td className="py-3 px-6">
                        <div className="flex items-center justify-center">
                            <div className="">
                                <img className="w-24 h-24" src="/initial/cristall.jpg"/>
                            </div>
                        </div>
                    </td>
                    <td className='py-3 px-6 text-justify'>
                        {this.state.articlePosts}
                    </td>
                    <td className="py-3 px-6 text-center">
                        <div className="flex item-center justify-center">
                            <div className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110 cursor-pointer cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                                </svg>
                            </div>
                            <div onClick={()=>this.setState({isOpenEditModal: true})} className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110 cursor-pointer cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                </svg>
                            </div>
                            <div onClick={()=>this.handleDeleteArticle()} className="w-4 mr-2 transform hover:text-purple-500 hover:scale-110 cursor-pointer cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                </svg>
                            </div>
                        </div>
                    </td>
                </tr>
                {
                    this.state.isOpenEditModal ? (
                        <>
                            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                                <div className="relative w-auto my-6 mx-auto max-w-3xl">
                                {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                    <h3 className="text-3xl font-semibold mr-6">
                                        Editer l&apos;Article{` ${this.props.id} `}
                                    </h3>
                                    <button className="p-1 ml-auto float-right" onClick={() => this.setState({isOpenEditModal : false})}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                    <form>
                                        <div className="mb-3 pt-0">
                                            <label>Titre de l&apos;Article</label>
                                            <input
                                                onChange={(event)=>{this.setState({articleTitreChange : event.target.value})}}
                                                value={this.state.articleTitreChange}
                                                type="text" 
                                                placeholder="" 
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Nom de l&apos;auteur de  l&apos;Article</label>
                                            <input
                                                onChange={()=>{}}
                                                value={this.state.articleAuteurChange}
                                                type="text" 
                                                placeholder="slug categorie" 
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Image de l&apos;Article</label>
                                            <input
                                                onChange={()=>{}}
                                                value={''}
                                                type="file"
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Appercu de l&apos;Article</label>
                                            <textarea value={this.state.articleAppercuChange} className="py-3 px-3 w-full text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none"></textarea>
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Contenu de l&apos;Article</label>
                                            <Editor contenu={this.state.articlePostsChange}/>
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Les categories de l&apos;article </label>
                                        </div>
                                    </form>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                                    <button
                                        className="text-red-500 background-transparent font-bold px-6 py-2 text-sm mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={() => this.setState({isOpenEditModal : false})}
                                    >
                                        Annuler
                                    </button>
                                    <button
                                        className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                        type="button"
                                        onClick={() =>{}}
                                    >
                                        Save Changes
                                    </button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                        </>
                    ):null
                }
            </>
        )
    }
}

export default AdminArticleItem