import React from 'react'
import AdminCategoryItem from './adminCategoryItem'
import {getAllCategory,deleteCategory,createCategory} from '../api/parseRequest'
import {formatSlug} from "../utils/formatSlug"


class AdminShowCategory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            categories : [],
            isOpenCreateModal : false,
            categoryNameChangeValue : '',
            categorySlugChangeValue : '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async deleteOneCategory (id,objectID) {
        //supression de la categorie avec l'identifiant objectID
        const result = await deleteCategory(objectID)
        //trouver la position de la catégorie à supprimer dans la variable this.state.catégories
        console.log(this.state.categories[id])
        console.log(objectID)
        console.log(result)
        const categories = await getAllCategory()
        this.setState({
            categories : categories,
        })
    }


    handleChange(event) {
        this.setState({
            categoryNameChangeValue : event.target.value,
            categorySlugChangeValue : formatSlug(event.target.value)
        })
    }

    async handleSubmit() {
        const attributes = {
            nom : this.state.categoryNameChangeValue,
            slug : this.state.categorySlugChangeValue
        }
        const categoryCreated = await createCategory(attributes)
        console.log(categoryCreated)
        const categories = await getAllCategory()
        this.setState({
            categories : categories,
            categoryNameChangeValue : '',
            categorySlugChangeValue : '',
        })
    }

    async componentDidMount () {
        const categories = await getAllCategory()
        this.setState({
            categories : categories,
        })
    }

    render () {
        return (
            <>
                <div className="flex justify-between items-center border-gray-300">
                    <h1 className="text-xl font-semibold  pt-2 pb-6">Categories</h1>
                    <button onClick={() =>this.setState({isOpenCreateModal : true})} className='bg-blue-500 p-2 text-white rounded capitalize'> 
                        Nouvelle Catégorie
                    </button>
                </div>
                <div className="bg-white shadow rounded-sm my-2.5 overflow-x-auto">
                    <table className="min-w-max w-full table-auto">
                        <thead>
                            <tr className="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th className="py-3 px-6 text-left">identifiant</th>
                                <th className="py-3 px-6 text-left">Nom</th>
                                <th className="py-3 px-6 text-left">Slug</th>
                                <th className="py-3 px-6 text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody className="text-gray-600 text-sm">
                            {this.state.categories.map((item,index)=>{
                                return <AdminCategoryItem handleDeleted={()=>this.deleteOneCategory(index,item.id)} nom={item.get('nom')} slug={item.get('slug')} id={item.id} key={item.id} />
                            })}
                        </tbody>
                    </table> 
                    {this.state.isOpenCreateModal ? (
                        <>
                        <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                            <div className="relative w-auto my-6 mx-auto max-w-3xl">
                            {/*content*/}
                                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                    {/*header*/}
                                    <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                        <h3 className="text-xl font-semibold">
                                            Créer une Catégorie
                                        </h3>
                                        <button onClick={() =>this.setState({isOpenCreateModal: false}) } className="p-1 ml-auto float-right">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                            </svg>
                                        </button>
                                    </div>
                                    {/*body*/}
                                    <div className="relative p-6 flex-auto">
                                    <form>
                                        <div className="mb-3 pt-0">
                                            <label>Nom de la categorie</label>
                                            <input
                                                onChange={(event)=>this.handleChange(event)}
                                                value={this.state.categoryNameChangeValue}
                                                type="text" 
                                                placeholder="Nom categorie" 
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                        <div className="mb-3 pt-0">
                                            <label>Slug de la categorie</label>
                                            <input
                                                onChange={()=>{}}
                                                value={this.state.categorySlugChangeValue}
                                                type="text" 
                                                placeholder="slug categorie" 
                                                className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none w-full"
                                            />
                                        </div>
                                    </form>
                                    </div>
                                    {/*footer*/}
                                    <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                                        <button className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button" onClick={() =>this.setState({isOpenCreateModal: false})}>
                                            Annuler
                                        </button>
                                        <button  
                                            className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" 
                                            type="button"
                                            onClick={() =>{
                                                this.handleSubmit()
                                                this.setState({isOpenCreateModal: false})
                                            }}
                                        >
                                            Ajouter cette categorie
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                        </>
                    ) : null}
                </div>
            </>
        )
    }
}

export default AdminShowCategory